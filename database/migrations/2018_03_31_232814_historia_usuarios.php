<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HistoriaUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historia_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_proyecto')->unsigned();
            $table->integer('id_estado');
            $table->string('rol_historia_usuario',40);
            $table->string('carac_funcionalidad',100);
            $table->string('razon_resuelto',100);
            $table->text('criterio_aceptacion');
            $table->integer('esfuerzo');
            $table->string('prioridad',20);
            $table->string('usuario_crea',40);
            $table->string('usuario_modifica',40)->nullable();
            $table->timestamps();

            $table->foreign('id_proyecto')
                  ->references('id')
                  ->on('proyecto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historia_usuarios');
    }
}
