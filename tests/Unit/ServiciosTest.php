<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ServiciosTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /*public function testExample()
    {
        $this->assertTrue(true);
    }*/

    public function testServicioProyectos(){

    	$estrutura = [[
    		'id',
    		'id_estado',
    		'nombre_proyecto',
    		'estim_spring_esfuerzo',
    		'usuario_crea',
    		'usuario_modifica',
    		'created_at',
    		'updated_at',
    		'dias_sprint',
    		'estado' => [
    			'id',
    			'tipo',
    			'estado'
    		]

    	]];

    	$respuesta = $this->json('GET', '/api/proyectos');

    	$respuesta
            ->assertStatus(200)
           	->assertJsonStructure($estrutura);
    	
    }

    public function testServicioProyecto(){

    	$estrutura = [
    		'id',
    		'id_estado',
    		'nombre_proyecto',
    		'estim_spring_esfuerzo',
    		'usuario_crea',
    		'usuario_modifica',
    		'created_at',
    		'updated_at',
    		'dias_sprint',
    		'estado' => [
    			'id',
    			'tipo',
    			'estado'
    		]

    	];

    	$respuesta = $this->json('GET', '/api/proyectos/3');

    	$respuesta
            ->assertStatus(200)
           	->assertJsonStructure($estrutura);
    	
    }

    public function testServicioHistorias(){

    	$estrutura = [[
    		'id',
    		'id_proyecto',
    		'id_estado',
    		'rol_historia_usuario',
    		'carac_funcionalidad',
    		'razon_resuelto',
    		'criterio_aceptacion',
    		'esfuerzo',
    		'prioridad',
    		'usuario_crea',
    		'usuario_modifica',
    		'created_at',
    		'updated_at',
    		'estado' => [
    			'id',
    			'tipo',
    			'estado'
    		],
    		'proyecto' => [
    			'id',
	    		'id_estado',
	    		'nombre_proyecto',
	    		'estim_spring_esfuerzo',
	    		'usuario_crea',
	    		'usuario_modifica',
	    		'created_at',
	    		'updated_at',
	    		'dias_sprint'
    		]	
    	]];

    	$respuesta = $this->json('GET', '/api/historias/2');

    	$respuesta
            ->assertStatus(200)
           	->assertJsonStructure($estrutura);

    }

    public function testServicioHistoria(){

    	$estrutura = [[
    		'id',
    		'id_proyecto',
    		'id_estado',
    		'rol_historia_usuario',
    		'carac_funcionalidad',
    		'razon_resuelto',
    		'criterio_aceptacion',
    		'esfuerzo',
    		'prioridad',
    		'usuario_crea',
    		'usuario_modifica',
    		'created_at',
    		'updated_at',
    		'estado' => [
    			'id',
    			'tipo',
    			'estado'
    		],
    		'proyecto' => [
    			'id',
	    		'id_estado',
	    		'nombre_proyecto',
	    		'estim_spring_esfuerzo',
	    		'usuario_crea',
	    		'usuario_modifica',
	    		'created_at',
	    		'updated_at',
	    		'dias_sprint'
    		]	
    	]];

    	$respuesta = $this->json('GET', '/api/historia/3');

    	$respuesta
            ->assertStatus(200)
           	->assertJsonStructure($estrutura);

    }

    
}
