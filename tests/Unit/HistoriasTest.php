<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HistoriasTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHistorias(){
    	
    	$respuesta = $this->get('/historias/2');
        $respuesta->assertStatus(200);
        $respuesta->assertSeeText('ProductBacklog');
        $respuesta->assertSeeText('Rol Historia Usuario');
        
    }

    public function testHistoriasFormulario(){
    	
    	$respuesta = $this->get('/crear-historia/3');
        $respuesta->assertStatus(200);
        $respuesta->assertSeeText('ProductBacklog');
        $respuesta->assertSeeText('Proyecto');
        $respuesta->assertSeeText('Estado');
        $respuesta->assertSeeText('Rol Historia Usuario');
        $respuesta->assertSeeText('Caracteristicas Funcionalidad');
        $respuesta->assertSeeText('Razon Resuelto');
        $respuesta->assertSeeText('Criterio Aceptacion');
        $respuesta->assertSeeText('Esfuerzo');
        $respuesta->assertSeeText('Prioridad');
        $respuesta->assertSeeText('Usuario Creador');
        $respuesta->assertSeeText('Guardar');
        $respuesta->assertSee('/proyectos');
    }
}
