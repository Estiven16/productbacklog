<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProyectosTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testProyecto(){
    	
    	$respuesta = $this->get('/proyectos');
        $respuesta->assertStatus(200);
        $respuesta->assertSeeText('ProductBacklog');
        
        
    }

    public function testProyectoFormulario(){

    	$respuesta = $this->get('/crear-proyecto');
        $respuesta->assertStatus(200);
        $respuesta->assertSeeText('ProductBacklog');
        $respuesta->assertSeeText('Crear Proyectos');
        $respuesta->assertSeeText('Nombre Proyecto');
        $respuesta->assertSeeText('Estimado Sprint Esfuerzo');
        $respuesta->assertSeeText('Días Sprint');
        $respuesta->assertSeeText('Usuario Creador');
        $respuesta->assertSeeText('Estado');

    }
}
