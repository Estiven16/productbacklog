<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
	protected $table = 'proyecto';
    protected $fillable = ['nombre_proyecto','id_estado','usuario_crea','usuario_modifica','estim_spring_esfuerzo','dias_sprint'];
	
	public function estado()
	{
		return $this->hasOne('App\Estado','id','id_estado')->where('tipo','P');
	}
}
