<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;
use Illuminate\Support\Facades\Redirect;

class IntegracionLoginController extends Controller
{
    public function autenticar(Request $rq){


    	$client = new Client([
		    // Base URI is used with relative requests
		    'base_uri' => 'http://10.10.28.164:8000/',
		    //'base_uri' => 'https://jsonplaceholder.typicode.com/',
		    // You can set any number of default request options.
		    //'timeout'  => 5.0,
		]);

		$respuesta = $client->request('GET', '/admin/service_login/?usuario='.$rq->usuario.'&clave='.$rq->password);
    	//$respuesta = $client->request('GET', 'posts');
		
		
		if ($respuesta->getStatusCode()==200){
			$respuesta = json_decode($respuesta->getBody()->getContents());
			if (isset($respuesta->error)) {
				dd($respuesta);
			}
			//dd($respuesta);
			
			session(['Id' => $respuesta[0]->Id]);
			session(['usuario' => $respuesta[0]->Usuario]);
			session(['rol' => $respuesta[0]->rol]);
			session(['Id_rol' => $respuesta[0]->Id_rol]);
			Redirect::to('/proyectos')->send();
		}else{
            Redirect::to('/')->send();
		}

		//dd(json_decode($respuesta->getBody()->getContents()));
    	//print_r($rq->all());

    }
}
