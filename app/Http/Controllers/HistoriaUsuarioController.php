<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
//modelos
use App\Proyecto;
use App\Estado;
use App\HistoriaUsuario;
//validaciones
use App\Http\Requests\HistoriaUsuariosRequest;

class HistoriaUsuarioController extends Controller
{
    public function __construct()
    {
        
       /*if(!session('id_usuario'))
       {
           Redirect::to('/')->send();
       }*/
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_proyecto)
    {
        $historias = HistoriaUsuario::where('id_proyecto',$id_proyecto)->with('estado')->with('proyecto')->get();
        return view('HistoriaUsuarios.HistoriaUsuarios',compact('historias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_proyecto)
    {
        $estados = Estado::where('tipo','H')->get();
        $proyecto = Proyecto::where('id',$id_proyecto)->first();
        //dd($proyecto);
        return view('HistoriaUsuarios.CrearHistoriaUsuario',compact('estados','proyecto'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $historia = new HistoriaUsuario;

        $data = [
            'id_proyecto'=>$request->id_proyecto,
            'id_estado'=>$request->id_estado,
            'rol_historia_usuario'=>$request->rol_historia_usuario,
            'carac_funcionalidad'=>$request->carac_funcionalidad,
            'razon_resuelto'=>$request->razon_resuelto,
            'criterio_aceptacion'=>$request->criterio_aceptacion,
            'esfuerzo'=>$request->esfuerzo,
            'prioridad'=>$request->prioridad,
            'usuario_crea'=>$request->usuario_crea
        ];

        $historia->fill($data);
        $historia->save();
        $request->session()->flash('alert-success', 'La historia de usuario fue creada');
        return Redirect::to('historias/'.$request->id_proyecto)->send();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estados = Estado::where('tipo','H')->get();
        $historia = HistoriaUsuario::where('id',$id)->first();
        $proyecto = Proyecto::where('id',$historia->id_proyecto)->first();
        //dd($historia);
        return view('HistoriaUsuarios.EditarHistoriaUsuario',compact('estados','proyecto','historia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $historia = new HistoriaUsuario;

        $data = [
            'id_proyecto'=>$request->id_proyecto,
            'id_estado'=>$request->id_estado,
            'rol_historia_usuario'=>$request->rol_historia_usuario,
            'carac_funcionalidad'=>$request->carac_funcionalidad,
            'razon_resuelto'=>$request->razon_resuelto,
            'criterio_aceptacion'=>$request->criterio_aceptacion,
            'esfuerzo'=>$request->esfuerzo,
            'prioridad'=>$request->prioridad,
            'usuario_crea'=>$request->usuario_crea
        ];

        $historia->where('id',$id)->update($data);
        $request->session()->flash('alert-success', 'La historia de usuario fue editado');
        return Redirect::to('historias/'.$request->id_proyecto)->send();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $rq)
    {
        $historias = new HistoriaUsuario;
        $historias->where('id',$rq->id)->delete();

        $rq->session()->flash('alert-success', 'Las Historias de Usuarios fueron Eliminadas');

        $response = array(
            'status' => 'success',
        );

        return \Response::json($response);
    }
}
