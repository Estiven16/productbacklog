<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
//modelos
use App\Proyecto;
use App\Estado;
use App\HistoriaUsuario;
//validaciones
use App\Http\Requests\ProyectosRequest;

class ProyectoController extends Controller
{
    public function __construct()
    {

       /*if(!session()->has('id_usuario'))
       {
            //session('id_usuario')
           Redirect::to('/')->send();
       }*/
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyectos = Proyecto::with('estado')->get();
        return view('Proyectos.proyectos',compact('proyectos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estados = Estado::where('tipo','P')->get();
        return view('Proyectos.CrearProyectos',compact('estados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProyectosRequest $request)
    {
        $proyecto = new Proyecto;
        //$config = new Config;
        $data = [
            'nombre_proyecto'=>$request->nombre_proyecto,
            'id_estado'=>$request->id_estado,
            'estim_spring_esfuerzo'=>$request->estim_spring_esfuerzo,
            'usuario_crea'=>$request->usuario_crea,
            'dias_sprint'=>$request->dias_sprint
        ];
        
        $proyecto->fill($data);
        $proyecto->save();
        $request->session()->flash('alert-success', 'El proyecto fue creado');
        return Redirect::to('proyectos')->send();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = Proyecto::where('id',$id)->first();
        $estados = Estado::where('tipo','P')->get();
        return view('Proyectos.EditarProyectos',compact('proyecto','estados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $proyecto = new Proyecto;

        $data = [
            'nombre_proyecto'=>$request->nombre_proyecto,
            'id_estado'=>$request->id_estado,
            'estim_spring_esfuerzo'=>$request->estim_spring_esfuerzo,
            'dias_sprint'=>$request->dias_sprint,
            'usuario_crea'=>$request->usuario_crea
        ];

        $proyecto->where('id',$id)->update($data);
        $request->session()->flash('alert-success', 'El proyecto fue editado');
        return Redirect::to('proyectos')->send();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $rq)
    {
        $proyecto = new Proyecto;
        $historias = new HistoriaUsuario;
        $historias->where('id_proyecto',$rq->id)->delete();
        $proyecto->where('id',$rq->id)->delete();
        $rq->session()->flash('alert-success', 'El proyecto y las Historias Asociadas fueron Eliminadas');
        //HelperApp($config);

        $response = array(
            'status' => 'success',
        );

        return \Response::json($response);
    }
}
