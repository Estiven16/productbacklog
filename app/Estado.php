<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    //nombre de la tabla
    protected $table = 'estados';

    protected $fillable = ['id','tipo','estado'];
 	
 	public function proyecto()
    {
        return $this->hasMany('App/Proyecto');

    }

    public function historia()
    {
        return $this->hasMany('HistoriaUsuario', 'id_estado');
    }
}
