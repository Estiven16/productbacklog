<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoriaUsuario extends Model
{
	protected $tabla = 'historia_usuario';
	protected $fillable = ['id_proyecto','id_estado','rol_historia_usuario','carac_funcionalidad','razon_resuelto','criterio_aceptacion','esfuerzo','prioridad','usuario_crea','usuario_modifica'];


    public function estado()
	{
		return $this->hasOne('App\Estado','id','id_estado')->where('tipo','H');
	}

	public function proyecto(){
		return $this->belongsTo('App\Proyecto','id_proyecto','id');
	}
}
