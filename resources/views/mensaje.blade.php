 <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info','error'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div> <!-- end .flash-message -->
  @if($errors->has(''))
      <div class="alert alert-danger" role="alert">
         @foreach ($errors->all() as $error)
            <div>{{ $error }}</div>
        @endforeach
      </div>
  @endif 
</br>  