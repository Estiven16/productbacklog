<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name','PRODUCT BACKLOG') }}</title>
    <!-- Styles -->
    <link href="{{ URL::to('/css/estilo.css') }}" rel="stylesheet">
    <link href="{{ URL::to('/css/app.css') }}" rel="stylesheet">
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">-->
    <link href="{{ URL::to('/js/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <!-- Scripts -->
    <script src="{{ URL::to('/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('/js/datatables.min.js') }}"></script>
    <!--<script src="{{ URL::to('/js/jquery.dataTables.min.js') }}"></script>-->
    <script src="{{ URL::to('/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('/js/bootbox.js') }}"></script>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top header-appvalue">
            <div class="container">
                <div class="">
                    <a class="navbar-brand" href="{{ url('/home') }}">
                        {{ config('app.name', 'ADMINISTRADOR APP VALUE') }}
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        <li class="dropdown">
                                <a href="#" class="dropdown-toggle" style="color:white!important" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ session('usuario').' '.session('rol') }} <span class="caret"></span>
                                </a>
                            </li>
                        @if (Auth::guest())

                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
       <!-- @yield('breadcrumbs')-->
        @yield('content')
    <!--<footer class="footer ">
            <div class="footer-left">&copy {{ date('Y') }} Todos los derechos reservados</div><div class="footer-right"></div>
    </footer>-->
</body>
</html>
