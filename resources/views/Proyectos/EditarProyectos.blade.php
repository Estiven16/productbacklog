@extends('layout.app')
@section('content')
@include('menu')
<div class="contenedor-tabla">
    <div class="title">Editar Proyectos</div>
    <hr>
@include('mensaje')
        @if ( count( $errors ) > 0 )
            <div class="alert alert-warning" role="alert">
               @foreach ($errors->all() as $error)
                  <div>{{ $error }}</div>
              @endforeach
            </div>
        @endif </br>
        <form action="{{ URL::to('editar-proyecto/'.$proyecto->id) }}" method="POST">
        {!! csrf_field() !!}
	        <div class="form-group">
	        	<label for="nombre_proyecto" class="control-label">Nombre Proyecto</label>
                <div>
                    <input type="text" name="nombre_proyecto" class="form-control" value="{{ $proyecto->nombre_proyecto }}" placeholder="escribir" required="">
                    <span class="help-block">
                        <strong>{{ $errors->first('nombre_proyecto') }}</strong>
                    </span>
                </div>
	        </div>
            <div class="form-group">
                <label for="estim_spring_esfuerzo" class="control-label">Estimado Sprint Esfuerzo</label>
                <div>
                    <input type="text" name="estim_spring_esfuerzo" class="form-control" value="{{ $proyecto->estim_spring_esfuerzo }}" placeholder="escribir" required="">
                    <span class="help-block">
                        <strong>{{ $errors->first('estim_spring_esfuerzo') }}</strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="estim_spring_esfuerzo" class="control-label">Días Sprint</label>
                <div>
                    <input type="text" name="dias_sprint" class="form-control" value="{{ $proyecto->dias_sprint }}" placeholder="escribir" required="">
                    <span class="help-block">
                        <strong>{{ $errors->first('dias_sprint') }}</strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="video" class="control-label">Usuario Creador</label>
                <div>
                    <input type="text" name="usuario_crea" class="form-control" value="{{ $proyecto->usuario_crea }}" placeholder="escribir" required="">
                    <span class="help-block">
                        <strong>{{ $errors->first('usuario_crea') }}</strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="estado" class="control-label">Estado</label>
                <div>
                    <select id="id_estado"  name='id_estado' class="form-control">
                    @foreach($estados as $estado)
                    	@if($estado->id==$proyecto->id_estado)
						<option value="{{ $estado->id }}" selected=>{{ $estado->estado }}</option>
						@else
						<option value="{{ $estado->id }}">{{ $estado->estado }}</option>
						@endif
                    @endforeach
                    </select>
                    <span class="help-block">
                        <strong>{{ $errors->first('id_estado') }}</strong>
                    </span>
                </div>
            </div>
    		<div class="form-group">
                <div class="col-md-6 col-md-offset-4">
        			<a href="{{URL::to('proyectos/')}}" class="btn btn-default">Atrás</a>
        			<button class="btn btn-default" type="submit">Guardar</button>
        		</div>
            </div>
		</form>
        <br>
</div>
@endsection