@extends('layout.app')
@section('content')
@include('menu')
<div class="container">
<div class="row">
<div class="col-md-10">
@include('mensaje')
        	<table id="tabla" class="stripe" cellspacing="0" width="100%">
	        <thead>
	            <tr>
	                <th>Nombre Proyecto</th>
	            	<th>Estado</th>
	            	<th>Estimado Spring Esfuerzo</th>
	            	<th>Usuario Creador</th>
	                <th></th>
	            </tr>
	        </thead>
	        @foreach($proyectos as $proyecto)
				<tr>
					<td>{{ $proyecto->nombre_proyecto }}</td>
					<td>{{ $proyecto->estado->estado }}</td>
					<td>{{ $proyecto->estim_spring_esfuerzo }}</td>
					<td>{{ $proyecto->usuario_crea }}</td>
					<td>
						<div class="dropdown">
						  <a class="dropdown-toggle" style="cursor:pointer;" id="dropdownMenu1{{$proyecto->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<i class="material-icons">more_vert</i>
						  </a>
						  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1{{$proyecto->id}}">
						  	<li><a href="{{URL::to('/historias/'.$proyecto->id) }}">Historias Usuarios</a></li>
						  	<li><a href="{{URL::to('/crear-historia/'.$proyecto->id) }}">Crear Historias Usuarios</a></li>
							<li><a href="{{URL::to('/editar-proyecto/'.$proyecto->id) }}">Editar</a></li>
							<li><button onclick="eliminar(this.value)" class="btn-menu-opciones" value="{{$proyecto->id}}">Eliminar</button></li>
						  </ul>
						</div>
					</td>
				</tr>
			@endforeach
			</tbody>
    	</table>
</div>
</div>
</div>
<script type="text/javascript">
   function eliminar(id){
		bootbox.confirm("¿Está seguro que desea eliminar éste proyecto?, se eliminara toda información relacionada con éste proyecto", function(result) {
			if(result){
			  	$.ajax({
					url: "{{ URL::to('/eliminar-proyecto') }}",
					type: 'DELETE',
					dataType: "JSON",
					data: {
						"_method": 'DELETE',
						"_token": '{!! csrf_token() !!}',
						"id":id,
					},
					success: function ()
					{
						location.reload();
					}
				});
			}
		});
	}
</script>
@endsection