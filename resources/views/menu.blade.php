<nav class="navbar navbar-default sidebar" role="navigation">
    <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!--<li class="active"><a href="{{ URL::to('/') }}">INICIO</a></li>-->
        <!--<li class="dropdown"><a href="{{ URL::to('/usuarios-admin') }}">USUARIOS ADMIN</a></li>-->
        <!--<li class="dropdown"><a href="{{ URL::to('/proveedores') }}">PROVEEDORES</a></li>-->
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">PROYECTOS<i class="fa fa-fw fa-caret-down"></i></a>
          <ul class="dropdown-menu forAnimate" role="menu">
            <li><a href="{{ URL::to('proyectos') }}">Ver Proyectos</a></li>
            <li><a href="{{ URL::to('crear-proyecto') }}">Crear Proyectos</a></li>
            <!--<li><a href="{{ URL::to('tecnologias/1') }}">Tecnologías</a></li>
            <li><a href="{{ URL::to('productos/1') }}">Productos</a></li>
            <li><a href="{{ URL::to('productos-otros/1') }}">Productos otras marcas</a></li>
            <li><a href="{{ URL::to('imeis/1') }}">IMEIS</a></li>
            <li><a href="{{ URL::to('comparaciones/1') }}">Reportes</a></li>-->
          </ul>
        </li>
        <!--<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">APP 2<i class="fa fa-fw fa-caret-down"></i></a>
          <ul class="dropdown-menu forAnimate" role="menu">
            <li><a href="{{ URL::to('usuarios-vendedores/2') }}">Usuarios / Vendedores</a></li>
            <li><a href="{{ URL::to('categorias/2') }}">Categorías</a></li>
            <li><a href="{{ URL::to('tecnologias/2') }}">Tecnologías</a></li>
            <li><a href="{{ URL::to('productos/2') }}">Productos</a></li>
            <li><a href="{{ URL::to('productos-otros/2') }}">Productos otras marcas</a></li>
            <li><a href="{{ URL::to('imeis/2') }}">IMEIS</a></li>
            <li><a href="{{ URL::to('comparaciones/2') }}">Reportes</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">APP 3<i class="fa fa-fw fa-caret-down"></i></a>
          <ul class="dropdown-menu forAnimate" role="menu">
            <li><a href="{{ URL::to('usuarios-vendedores/3') }}">Usuarios / Vendedores</a></li>
            <li><a href="{{ URL::to('categorias/3') }}">Categorías</a></li>
            <li><a href="{{ URL::to('tecnologias/3') }}">Tecnologías</a></li>
            <li><a href="{{ URL::to('productos/3') }}">Productos</a></li>
            <li><a href="{{ URL::to('productos-otros/3') }}">Productos otras marcas</a></li>
            <li><a href="{{ URL::to('imeis/3') }}">IMEIS</a></li>
            <li><a href="{{ URL::to('comparaciones/3') }}">Reportes</a></li>
          </ul>
        </li>-->
      </ul>
    </div>
  </div>
</nav>