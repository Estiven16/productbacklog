@extends('layout.app')
@section('content')
@include('menu')
<div class="contenedor-tabla">
    <div class="title">Editar Historia Usuario</div>
    <hr>
@include('mensaje')
        @if ( count( $errors ) > 0 )
            <div class="alert alert-warning" role="alert">
               @foreach ($errors->all() as $error)
                  <div>{{ $error }}</div>
              @endforeach
            </div>
            <br>
        @endif 
        <form action="{{ URL::to('crear-historia') }}" method="POST">
        {!! csrf_field() !!}
	        <div class="form-group">
	        	<label for="" class="control-label">Proyecto</label>
                <div>
                    <input type="text" class="form-control" value="{{ $proyecto->nombre_proyecto }}" disabled="">
                    <input type="hidden" name="id_proyecto" value='{{ $proyecto->id }}'>
                </div>
	        </div>
	        <div class="form-group">
                <label for="estado" class="control-label">Estado</label>
                <div>
                    <select id="id_estado"  name='id_estado' class="form-control">
                    @foreach($estados as $estado)
						<option value="{{ $estado->id }}">{{ $estado->estado }}</option>
                    @endforeach
                    </select>
                    <span class="help-block">
                        <strong>{{ $errors->first('id_estado') }}</strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="rol_historia_usuario" class="control-label">Rol Historia Usuario</label>
                <div>
                    <input type="text" name="rol_historia_usuario" class="form-control" value="" placeholder="escribir" required="">
                    <span class="help-block">
                        <strong>{{ $errors->first('rol_historia_usuario') }}</strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="carac_funcionalidad" class="control-label">Caracteristicas Funcionalidad</label>
                <div>
                    <input type="text" name="carac_funcionalidad" class="form-control" value="" placeholder="escribir" required="">
                    <span class="help-block">
                        <strong>{{ $errors->first('carac_funcionalidad') }}</strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="razon_resuelto" class="control-label">Razon Resuelto</label>
                <div>
                    <input type="text" name="razon_resuelto" class="form-control" value="" placeholder="escribir" required="">
                    <span class="help-block">
                        <strong>{{ $errors->first('') }}</strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="criterio_aceptacion" class="control-label">Criterio Aceptacion</label>
                <div>
                    <textarea name="criterio_aceptacion" rows="10" class="form-control" value="" placeholder="escribir" required=""></textarea>
                    <span class="help-block">
                        <strong>{{ $errors->first('criterio_aceptacion') }}</strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="esfuerzo" class="control-label">Esfuerzo</label>
                <div>
                    <input type="text" name="esfuerzo" class="form-control" value="" placeholder="escribir" required="">
                    <span class="help-block">
                        <strong>{{ $errors->first('esfuerzo') }}</strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="esfuerzo" class="control-label">Prioridad</label>
                <div>
                    <input type="text" name="prioridad" class="form-control" value="" placeholder="escribir" required="">
                    <span class="help-block">
                        <strong>{{ $errors->first('prioridad') }}</strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label for="video" class="control-label">Usuario Creador</label>
                <div>
                    <input type="text" name="usuario_crea" class="form-control" value="" placeholder="escribir" required="">
                    <span class="help-block">
                        <strong>{{ $errors->first('usuario_crea') }}</strong>
                    </span>
                </div>
            </div>
    		<div class="form-group">
                <div class="col-md-6 col-md-offset-4">
        			<a href="{{URL::to('proyectos/')}}" class="btn btn-default">Atrás</a>
        			<button class="btn btn-default" type="submit">Guardar</button>
        		</div>
            </div>
		</form>
		<br>
</div>
@endsection