@extends('layout.app')
@section('content')
@include('menu')
<div class="container">
<div class="row">
<div class="col-md-10">
@include('mensaje')
        	<table id="tabla" class="stripe" cellspacing="0" width="100%">
	        <thead>
	            <tr>
	                <th>Proyecto</th>
	            	<th>Estado</th>
	            	<th>Rol Historia Usuario</th>
	            	<th>Caracteristicas de Funcionalidad</th>
	                <th></th>
	            </tr>
	        </thead>
	        @foreach($historias as $historia)
				<tr>
					<td>{{ $historia->proyecto->nombre_proyecto }}</td>
					<td>{{ $historia->estado->estado }}</td>
					<td>{{ $historia->rol_historia_usuario }}</td>
					<td>{{ $historia->carac_funcionalidad }}</td>
					<td>
						<div class="dropdown">
						  <a class="dropdown-toggle" style="cursor:pointer;" id="dropdownMenu1{{$historia->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<i class="material-icons">more_vert</i>
						  </a>
						  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1{{$historia->id}}">
							<li><a href="{{URL::to('/editar-historia/'.$historia->id) }}">Editar</a></li>
							<li><button onclick="eliminar(this.value)" class="btn-menu-opciones" value="{{$historia->id}}">Eliminar</button></li>
						  </ul>
						</div>
					</td>
				</tr>
			@endforeach
			</tbody>
    	</table>
</div>
</div>
</div>
<script type="text/javascript">
   function eliminar(id){
		bootbox.confirm("¿Está seguro que desea eliminar esta Historia de Usuario?, se eliminara toda información relacionada con esta Historia", function(result) {
			if(result){
			  	$.ajax({
					url: "{{ URL::to('/eliminar-historia') }}",
					type: 'DELETE',
					dataType: "JSON",
					data: {
						"_method": 'DELETE',
						"_token": '{!! csrf_token() !!}',
						"id":id,
					},
					success: function ()
					{
						location.reload();
					}
				});
			}
		});
	}
</script>
@endsection