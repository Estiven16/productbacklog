<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/autenticar', 'IntegracionLoginController@autenticar');
//proyectos
Route::get('/proyectos', 'ProyectoController@index');
Route::get('/crear-proyecto','ProyectoController@create');
Route::post('/crear-proyecto','ProyectoController@store');
Route::get('/editar-proyecto/{id}','ProyectoController@edit');
Route::post('/editar-proyecto/{id}','ProyectoController@update');
Route::delete('/eliminar-proyecto','ProyectoController@destroy');

//historias
Route::get('/historias/{id_proyecto}', 'HistoriaUsuarioController@index');
Route::get('/crear-historia/{id_proyecto}', 'HistoriaUsuarioController@create');
Route::post('/crear-historia', 'HistoriaUsuarioController@store');
Route::get('/editar-historia/{id_historia}', 'HistoriaUsuarioController@edit');
Route::post('/editar-historia/{id_historia}', 'HistoriaUsuarioController@update');
Route::delete('/eliminar-historia','HistoriaUsuarioController@destroy');

